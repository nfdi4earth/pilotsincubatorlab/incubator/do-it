document.addEventListener("DOMContentLoaded", () => {
  const canvas = document.getElementById("gridCanvas");
  const context = canvas.getContext("2d");
  const magnifierCanvas = document.getElementById("magnifierCanvas");
  const magnifierContext = magnifierCanvas.getContext("2d");
  const magnifier = document.querySelector(".magnifier");
  const cellSize = 1;
  const topBottomPadding = 100;

  let isDrawingActive = false;
  let isDrawing = false;
  const filledCells = [];
  let eraseMode = false;
  let zoomAlignedToggled = false;
  let alignOffsetX = 30;
  let alignOffsetY = -30;
  let isLineDrawingMode = false;
  let horizontalLinesYPositions = []; // Store Y positions of horizontal lines
  let verticalLinesXPositions = []; // Store X positions of vertical lines
  let isDraggingLine = null; // Tracks if a line is being dragged (0=horizontal, 1=vertical)

  // Control buttons
  const startRecordingButton = document.getElementById("startRecording");
  const stopRecordingButton = document.getElementById("stopRecording");
  const submitDataButton = document.getElementById("submitData");
  const toggleAddErase = document.getElementById("toggleAddErase");
  const toggleAddEraseText = document.getElementById("toggleAddErase_text");
  const zoomAligned = document.getElementById("zoomAligned");
  const zoomAlignedText = document.getElementById("zoomAligned_text");
  const magnifier_marker = document.getElementById("magnifier_marker");

  // Line move toggle
  const toggleLineMove = document.getElementById("toggleLineMove");

  // Set initial state
  magnifier.style.display = "none"; // Hide magnifier initially
  toggleLineMove.checked = false; // Ensure the line move toggle is deactivated

  // Initialize the two horizontal lines and two vertical lines
  function initializeLinePositions() {
    horizontalLinesYPositions = [canvas.height * 0.1, canvas.height * 0.9];
    verticalLinesXPositions = [canvas.width * 0.1, canvas.width * 0.9];
  }

  // Start drawing
  startRecordingButton.addEventListener("click", () => {
    isDrawingActive = true;
    magnifier.style.display = "block"; // Show magnifier

    //always start in draw mode
    eraseMode = false;
    toggleAddErase.checked = false;
    toggleAddEraseText.innerHTML = "Draw Mode";

    // Deactivate line move toggle when drawing starts
    toggleLineMove.checked = false;
    isLineDrawingMode = false; // Ensure line drawing mode is disabled
    canvas.style.cursor = "crosshair"; // Reset cursor back to normal
  });

  // Stop drawing
  stopRecordingButton.addEventListener("click", () => {
    isDrawingActive = false;
    isDrawing = false;
    magnifier.style.display = "none"; // Hide magnifier
  });

  // Submit data: Log filled cells and clear the canvas
  submitDataButton.addEventListener("click", () => {
    // Log the filled cells array
    console.log("Filled Cells:", Array.from(filledCells));

    // Log line information (Y positions for horizontal, X positions for vertical)
    console.log("Horizontal Line Y Positions:", horizontalLinesYPositions);
    console.log("Vertical Line X Positions:", verticalLinesXPositions);

    // Log values of all input fields
    const yUpValue = document.getElementById("y_up").value;
    const yLowValue = document.getElementById("y_low").value;
    const xLeValue = document.getElementById("x_le").value;
    const xReValue = document.getElementById("x_re").value;

    console.log(
      `y_up: ${yUpValue}, y_low: ${yLowValue}, x_le: ${xLeValue}, x_re: ${xReValue}`
    );

    // Clear the canvas
    clearCanvas(); // This will also clear the filledCells array

    // Reset the input fields
    document.getElementById("y_up").value = "";
    document.getElementById("y_low").value = "";
    document.getElementById("x_le").value = "";
    document.getElementById("x_re").value = "";

    // Reset line positions
    initializeLinePositions(); // Set to default positions
    drawLines(); // Redraw lines at their initial positions
  });

  // Toggle between draw mode and erase mode
  toggleAddErase.addEventListener("change", () => {
    eraseMode = toggleAddErase.checked;
    toggleAddEraseText.innerHTML = eraseMode ? "Erase mode" : "Draw mode";
  });

  // Toggle between zoom offset and aligned
  zoomAligned.addEventListener("change", () => {
    zoomAlignedToggled = zoomAligned.checked;
    zoomAlignedText.innerHTML = zoomAlignedToggled
      ? "Zoom aligned"
      : "Zoom offset";
  });

  // Toggle line drawing mode when the toggle is changed
  toggleLineMove.addEventListener("change", () => {
    isLineDrawingMode = toggleLineMove.checked;

    if (isLineDrawingMode) {
      magnifier.style.display = "none"; // Hide magnifier while moving the lines
      drawLines(); // Draw the lines at the current positions
      canvas.style.cursor = "ns-resize"; // Change cursor to double arrow for horizontal movement
      isDrawing = false; // Toggle drawing off when moving lines
    } else {
      canvas.style.cursor = "crosshair"; // Reset cursor back to normal
      drawLines(); // Ensure the lines are redrawn
    }
  });

  // Load the image on the canvas
  const img = new Image();
  img.src = "125_19951208_19951215.jpg";
  img.onload = () => {
    const imgWidth = img.width;
    const imgHeight = img.height;

    // Calculate scaled dimensions maintaining aspect ratio with top and bottom padding
    const containerHeight = window.innerHeight - 2 * topBottomPadding;
    const scaledHeight = containerHeight; // Scale to the available height
    const scaledWidth = scaledHeight * (imgWidth / imgHeight); // Maintain aspect ratio

    // Set canvas size to match the scaled image size
    canvas.width = scaledWidth;
    canvas.height = scaledHeight;
    canvas.style.width = `${scaledWidth}px`;
    canvas.style.height = `${scaledHeight}px`;

    // Clear the canvas and fill it with white
    context.fillStyle = "#FFFFFF";
    context.fillRect(0, 0, canvas.width, canvas.height);

    // Draw the initial image onto the canvas
    context.drawImage(img, 0, 0, scaledWidth, scaledHeight);

    // Set magnifier canvas size
    magnifierCanvas.width = 300; // Adjusted from 600
    magnifierCanvas.height = 300; // Adjusted from 600

    // Call this after image and canvas setup
    initializeLinePositions();

    // Draw the horizontal and vertical lines initially
    drawLines();

    // Initialize drawing on the canvas
    initializeDrawing();
  };

  function drawLine(xStart, yStart, xEnd, yEnd) {
    context.beginPath();
    context.moveTo(xStart, yStart);
    context.lineTo(xEnd, yEnd);
    context.lineWidth = 2;
    context.strokeStyle = "#ff6e48";
    context.stroke();
  }

  function drawLines() {
    // Draw horizontal lines
    horizontalLinesYPositions.forEach((yPosition) => {
      const lineWidth = canvas.width * 0.85; // Set line length to 0.85 of canvas width
      const xPadding = (canvas.width - lineWidth) / 2; // Centering the line
      drawLine(xPadding, yPosition, xPadding + lineWidth, yPosition);
    });

    // Draw vertical lines
    verticalLinesXPositions.forEach((xPosition) => {
      const lineHeight = canvas.height * 0.85; // Set line length to 0.85 of canvas height
      const yPadding = (canvas.height - lineHeight) / 2; // Centering the line
      drawLine(xPosition, yPadding, xPosition, yPadding + lineHeight); // Draw from top to bottom
    });

    redrawFilledCells(); // Redraw all filled cells after drawing the lines
  }

  // Redraw the filled cells
  function redrawFilledCells() {
    filledCells.forEach((cell) => {
      const [cellX, cellY] = cell.split(",").map(Number);
      context.fillStyle = "#000";
      context.fillRect(cellX, cellY, cellSize, cellSize);
    });
  }

  // Initialize drawing functionality
  function initializeDrawing() {
    // Get the cursor position relative to the canvas
    function getCursorPosition(event) {
      const rect = canvas.getBoundingClientRect();
      const scaleX = canvas.width / rect.width;
      const scaleY = canvas.height / rect.height;
      const mouseX = (event.clientX - rect.left) * scaleX;
      const mouseY = (event.clientY - rect.top) * scaleY;
      return { mouseX, mouseY };
    }

    // Handle mouse down event to initiate line dragging or toggle drawing
    canvas.addEventListener("mousedown", (event) => {
      const mouseY = event.clientY - canvas.getBoundingClientRect().top; // Get the Y position of the mouse
      const mouseX = event.clientX - canvas.getBoundingClientRect().left; // Get the X position of the mouse

      if (isLineDrawingMode) {
        // Check if mouse is near any horizontal line
        horizontalLinesYPositions.forEach((yPos, index) => {
          if (Math.abs(mouseY - yPos) < 5) {
            isDraggingLine = `h${index}`; // Allows tracking which horizontal line is being dragged
            magnifier.style.display = "none"; // Hide magnifier during line movement
            canvas.style.cursor = "ns-resize";
          }
        });

        // Check if mouse is near any vertical line
        verticalLinesXPositions.forEach((xPos, index) => {
          if (Math.abs(mouseX - xPos) < 5) {
            isDraggingLine = `v${index}`; // Allows tracking which vertical line is being dragged
            magnifier.style.display = "none"; // Hide magnifier during line movement
            canvas.style.cursor = "w-resize";
          }
        });
      } else if (isDrawingActive) {
        isDrawing = !isDrawing; // Toggle the drawing state
        magnifier_marker.classList.toggle("marker_color1");
        magnifier_marker.classList.toggle("marker_color2");
      }
    });

    // Handle mouse up event to stop drawing and dragging
    document.addEventListener("mouseup", () => {
      isDraggingLine = null; // Stop dragging the line
      if (!toggleLineMove.checked) {
        // Only show the magnifier if the line move toggle is not checked
        magnifier.style.display = "block"; // Show magnifier again when done
      }
    });

    // Handle mouse move events on the canvas
    canvas.addEventListener("mousemove", (event) => {
      if (isLineDrawingMode && isDraggingLine !== null) {
        const mouseY = event.clientY - canvas.getBoundingClientRect().top; // Get the Y position of the mouse
        const mouseX = event.clientX - canvas.getBoundingClientRect().left; // Get the X position of the mouse

        if (isDraggingLine.startsWith("h")) {
          // Moving horizontal lines
          const index = parseInt(isDraggingLine[1], 10);
          horizontalLinesYPositions[index] = Math.max(
            0,
            Math.min(canvas.height, mouseY)
          ); // Update the Y position of the dragged line
        } else if (isDraggingLine.startsWith("v")) {
          // Moving vertical lines
          const index = parseInt(isDraggingLine[1], 10);
          verticalLinesXPositions[index] = Math.max(
            0,
            Math.min(canvas.width, mouseX)
          ); // Update the X position of the dragged line
        }

        clearCanvas(false); // Preserve filled cells
        drawLines(); // Draw all lines at their new positions
        return; // Skip other drawing logic
      }

      if (!isDrawingActive) return;

      const { mouseX, mouseY } = getCursorPosition(event);

      if (isDrawing) {
        if (eraseMode) {
          eraseCell(mouseX, mouseY);
        } else {
          fillCell(mouseX, mouseY);
        }
      }

      const imgLeft = canvas.getBoundingClientRect().left;
      const imgTop = canvas.getBoundingClientRect().top;

      if (zoomAlignedToggled) {
        alignOffsetX = -77;
        alignOffsetY = -77;
      } else {
        alignOffsetX = 30;
        alignOffsetY = -30;
      }

      // Update magnifier position with a 30px offset to the top and right
      magnifier.style.left = `${event.clientX - imgLeft + alignOffsetX}px`;
      magnifier.style.top = `${event.clientY - imgTop + alignOffsetY}px`;

      // Update magnifier content
      magnifierContext.clearRect(
        0,
        0,
        magnifierCanvas.width,
        magnifierCanvas.height
      );
      magnifierContext.drawImage(
        canvas,
        mouseX - 37,
        mouseY - 37,
        150,
        150,
        0,
        0,
        magnifierCanvas.width,
        magnifierCanvas.height
      );
    });

    // Fill a cell at the specified coordinates
    function fillCell(mouseX, mouseY) {
      const cellX = Math.floor(mouseX / cellSize) * cellSize;
      const cellY = Math.floor(mouseY / cellSize) * cellSize;
      filledCells.push(`${cellX},${cellY}`); // Track filled cells
      context.fillStyle = "#000";
      context.fillRect(cellX, cellY, cellSize, cellSize);
    }

    // Erase a cell at the specified coordinates
    function eraseCell(mouseX, mouseY) {
      const centerX = Math.floor(mouseX / cellSize) * cellSize;
      const centerY = Math.floor(mouseY / cellSize) * cellSize;
      const cellsToErase = [
        [centerX, centerY],
        [centerX - cellSize, centerY - cellSize],
        [centerX, centerY - cellSize],
        [centerX + cellSize, centerY - cellSize],
        [centerX - cellSize, centerY],
        [centerX + cellSize, centerY],
        [centerX - cellSize, centerY + cellSize],
        [centerX, centerY + cellSize],
        [centerX + cellSize, centerY + cellSize],
      ];
      cellsToErase.forEach(([x, y]) => {
        const index = filledCells.indexOf(`${x},${y}`);
        if (index >= 0) {
          filledCells.splice(index, 1); // Remove from tracked cells
        }
        // Clear the cell on the canvas
        context.clearRect(x, y, cellSize, cellSize);
        // Redraw the portion of the image corresponding to this cell
        const imgWidth = img.width * (cellSize / canvas.width);
        const imgHeight = img.height * (cellSize / canvas.height);
        context.drawImage(
          img,
          (x / canvas.width) * img.width,
          (y / canvas.height) * img.height,
          imgWidth,
          imgHeight,
          x,
          y,
          cellSize,
          cellSize
        );
      });
    }
  }

  // Clear the canvas and reset the filled cells
  function clearCanvas(clearFilledCells = true) {
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.drawImage(img, 0, 0, canvas.width, canvas.height);
    if (clearFilledCells) {
      filledCells.length = 0; // Clear the filled cells tracking array
    }
  }
});
