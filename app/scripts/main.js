var prevScrollpos = window.scrollY;
var scrollThreshold = 200;
var bar;

window.onscroll = function () {
  var currentScrollPos = window.scrollY;
  bar = document.getElementById("navbar_blank");
  if (bar && Math.abs(prevScrollpos - currentScrollPos) > scrollThreshold) {
    if (prevScrollpos > currentScrollPos) {
      bar.style.opacity = 1;
      bar.classList.add("out_shad");
      bar.style.top = "0px";
    } else {
      bar.style.opacity = 0;
      bar.classList.remove("out_shad");
      bar.style.top = "-100px";
      document.getElementById("hamburger").checked = false;
    }
    prevScrollpos = currentScrollPos;
  }
};

function loadHeader() {
  fetch("header.html")
    .then((response) => response.text())
    .then((data) => {
      document.getElementById("navbar_blank").innerHTML = data;

      var currentPage = window.location.pathname.split("/").pop().split(".")[0];

      if (!currentPage) {
        currentPage = "index";
      }

      const navElement = document.getElementById(currentPage);
      if (navElement) {
        navElement.classList.add("active");
      }
    })
    .catch((error) => console.error("Error loading header:", error));
}

function loadFooter() {
  fetch("footer.html")
    .then((response) => response.text())
    .then((data) => {
      document.getElementById("footer_blank").innerHTML = data;
    })
    .catch((error) => console.error("Error loading header:", error));
}

function switchLanguage() {
  const supportedLangs = ["de", "en"];
  const defaultLang = "en";
  let currentUrl = window.location.href;

  // Ensure there's no trailing slash for consistent processing
  if (currentUrl.endsWith("/")) {
    currentUrl = currentUrl.slice(0, -1);
  }

  const urlParts = currentUrl.split("/");
  const currentLangIndex = urlParts.findIndex((part) =>
    supportedLangs.includes(part)
  );

  if (currentLangIndex > -1) {
    // Switch to the next language in the supportedLangs array
    const currentLang = urlParts[currentLangIndex];
    const newLangIndex =
      (supportedLangs.indexOf(currentLang) + 1) % supportedLangs.length;
    const newLang = supportedLangs[newLangIndex];

    urlParts[currentLangIndex] = newLang;
  } else {
    // Current language not found, default to inserting defaultLang after the domain
    urlParts.splice(3, 0, defaultLang); // Assuming 'http(s)://domain/' structure
  }

  const newUrl = urlParts.join("/");
  window.location.href = newUrl;
}

document.body.addEventListener("click", (event) => {
  if (event.target.closest("#langswitch_btn")) {
    switchLanguage();
  }
});

document.addEventListener("DOMContentLoaded", () => {
  loadHeader();
  loadFooter();
});
