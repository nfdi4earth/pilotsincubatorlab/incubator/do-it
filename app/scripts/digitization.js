var dataset_id = "";
var dataset_iv = "";

function isEnglishUrl() {
  let url = window.location.href;
  return url.includes("/en/");
}

// build identical input fields for each day of the month
function createDailyInputs(dayCounter, month) {
  var div = document.createElement("div");
  var checkbox = document.createElement("input");
  checkbox.type = "checkbox";
  checkbox.name = `${dayCounter}_not_readable`;
  checkbox.id = `${dayCounter}_not_readable`;
  checkbox.checked = true;
  checkbox.tabIndex = -1;

  const input = document.createElement("input");
  input.className = "input_bubble";
  let showDays = dayCounter.toString().padStart(2, "0");
  input.placeholder = `${showDays}. ${month}`;
  input.name = `${dayCounter}`;
  input.min = "0";
  input.step = "0.1";
  input.type = "number";
  input.required = true;

  div.appendChild(checkbox);
  div.appendChild(input);

  return div;
}

// create the 3 x 4 layout per grid cell
function createDailyInputsLayout(numDays, month) {
  let dayCounter = 1;

  // for each grid row
  for (let i = 1; i <= 3; i++) {
    const container = document.getElementById(`daily${i}`);
    container.innerHTML = ""; // Clear any existing inputs
    const innerContainer = document.createElement("div");
    innerContainer.className = "daily_inputs";

    // Determine the number of days to add to the current container
    let daysToAdd;
    if (i <= 2) {
      daysToAdd = 10; // First two containers always have 10 days
    } else {
      daysToAdd = numDays - 20; // Remaining days for the last container
    }

    // Add the individual daily inputs to the container
    for (let j = 1; j <= daysToAdd; j++) {
      if (dayCounter > numDays) break; // Stop if we've added all days
      var input_div = createDailyInputs(dayCounter, month);
      innerContainer.appendChild(input_div);
      dayCounter++;
    }

    // 28 days last row of 3rd grid cell stays empty
    if (numDays == 28 && i == 3) {
      var input_div = createDailyInputs("99", month);
      input_div.style.visibility = "hidden";

      // Set this input to text as it is only for visual purposes
      // otherwise postRequest is off
      var inputElement = input_div.querySelector("input[type='number']");
      if (inputElement) {
        inputElement.type = "text";
        inputElement.required = false;
      }

      innerContainer.appendChild(input_div);
      daysToAdd++;
    }

    var blanks = 12 - daysToAdd;
    // add in blanks to make always 4 bubbles per row
    for (let j = 1; j <= blanks; j++) {
      const div = document.createElement("div");
      div.style.visibility = "hidden";
      innerContainer.appendChild(div);
    }
    container.appendChild(innerContainer);
  }
}

function detect_disabled(value) {
  if (value == "") {
    return NaN;
  } else {
    return parseFloat(value);
  }
}

function compareDailyWithMonthlySum(daily, monthly) {
  daily = Math.round(daily * 100) / 100; //avoid floating problems
  return Math.abs(monthly - daily) < 0.2; // custom tolerance
}

//check sum in order to throw customvalidity, which gets triggered on submit of the form
const checkMonthlySumValidity = (event) => {
  console.log("Checking monthly sum validity");
  let sum = 0.0;
  document
    .querySelectorAll(".daily_inputs [type='number']")
    .forEach((el) => (sum += Number(el.value)));

  const sum_ele = document.querySelector("#monthly_sum");

  if (!compareDailyWithMonthlySum(sum, sum_ele.value)) {
    if (isEnglishUrl()) {
      let errorMessage =
        "Sum of transcribed days (" +
        sum.toFixed(2) + // Format the sum to two decimal points
        " mm) and the monthly sum don't match, please check. \nHowever, if everything is checked, deactivate this field.";
      sum_ele.setCustomValidity(errorMessage);
    } else {
      let errorMessage =
        " Die Summe der digitalisierten Tageswerte (" +
        sum.toFixed(2) + // Format the sum to two decimal points
        " mm) und die Monatssumme stimmen nicht überein, bitte prüfen. \nFalls allerdings alles korrekt ist, deaktivieren Sie dieses Feld";

      sum_ele.setCustomValidity(errorMessage);
    }
  } else {
    sum_ele.setCustomValidity("");
  }
};

async function sendPostRequest(post_body) {
  try {
    const response = await fetch("https://doit-lwi.tu-bs.de/api/data", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(post_body),
    });

    //ok is true for status codes 200-299
    if (!response.ok) {
      throw new Error(`HTTP ${response.status}`);
    }

    const result = await response.text();
    console.log("POST request successful:", result);
    return true;
  } catch (error) {
    console.error("Error in POST request:", error);
    if (isEnglishUrl()) {
      document.getElementById("error_element").innerHTML =
        "Something went wrong, please try again.";
    } else {
      document.getElementById("error_element").innerHTML =
        "Etwas ist schief gelaufen, bitte versuchen Sie es erneut.";
    }
    return false;
  }
}

function changeToThankYou() {
  if (isEnglishUrl()) {
    document.getElementById(
      "digi_form"
    ).innerHTML = `<div class="thank_container">
    <div class="thank_item" id="thank_item">
      <div class="head1">Thank you!</div>
      <p>We have made another stride towards our goal.</p>
      <button type="button" class="digi_button_nowid" id="restart_btn">
        Another digitization?
      </button>
    </div>
  </div>`;
  } else {
    document.getElementById(
      "digi_form"
    ).innerHTML = `<div class="thank_container">
    <div class="thank_item" id="thank_item">
      <div class="head1">Vielen Dank!</div>
      <p>Wir sind dem Ziel wieder ein Stück näher gekommen.</p>
      <button type="button" class="digi_button_nowid" id="restart_btn">
        Noch eine Digitalisierung?
      </button>
    </div>
  </div>`;
  }

  document.getElementById("moveArrow").remove();

  document
    .getElementById("restart_btn")
    .addEventListener("click", PrepareDataset);
}

function buildAndHandleDigitizationForm(days, month) {
  createDailyInputsLayout(days, month);

  document
    .querySelector("#monthly_sum")
    .addEventListener("change", checkMonthlySumValidity);

  document.querySelectorAll("[type='number']").forEach((el) => {
    el.addEventListener("change", checkMonthlySumValidity);
  });

  document.querySelector("form").addEventListener("submit", async (event) => {
    event.preventDefault(); //disable default page reload on submit

    // check validity again in case a day was changed after the last check
    document
      .querySelector("#monthly_sum")
      .dispatchEvent(new CustomEvent("change"));

    //send to API
    let results = [];
    document
      .querySelectorAll(".daily_inputs [type='number']")
      .forEach((el) => results.push(detect_disabled(el.value)));

    let monthly_sum = parseFloat(document.getElementById("monthly_sum").value);
    // reverse checked, because checked means it is readable
    let monthly_sum_not_readable =
      !document.getElementById("sum_not_readable").checked;
    let user_comment = document.getElementById("user-comment").value;

    post_body = {
      data_id: dataset_id,
      data_iv: dataset_iv,
      results: results,
      monthly_sum: monthly_sum,
      monthly_sum_not_readable: monthly_sum_not_readable,
      user_comment: user_comment,
    };

    let postSuccess = await sendPostRequest(post_body);
    if (postSuccess) {
      digiHasStarted = false;
      changeToThankYou();
    }
  });

  // disable number input when checkbox is unchecked and make checks untabbable
  document.querySelectorAll("[type='checkbox']").forEach((el) => {
    el.tabIndex = -1;

    el.addEventListener("change", (event) => {
      const numberElement = event.target.nextElementSibling;
      if (event.target.checked) {
        el.tabIndex = -1;
        numberElement.disabled = false;
        numberElement.focus();
      } else {
        numberElement.disabled = true;
      }
    });
  });

  // Disable number input when 'x' is pressed and field is empty
  document.querySelectorAll("[type='number']").forEach((el) => {
    el.addEventListener("keydown", (event) => {
      if (el.value == "" && event.key == "x") {
        el.disabled = true;
        event.target.previousElementSibling.tabIndex = 0;
        event.target.previousElementSibling.checked = false;
        event.target.previousElementSibling.focus();
      }
    });
  });
}

// handle all the zoomign functionality (setup + eventlisteners)
function imageZoom(imgID, resultID, lensID) {
  //https://javascript.info/mouse-drag-and-drop
  let cx, cy;

  const img = document.getElementById(imgID);
  const result = document.getElementById(resultID);
  const lens = document.getElementById(lensID);
  const arrow = document.getElementById("moveArrow");
  const arrow_height = arrow.offsetHeight;
  const arrow_width = arrow.offsetWidth;

  // border width is used to make a visual and functional valid alignment
  lens_border_width = getComputedStyle(lens, null).getPropertyValue(
    "border-left-width"
  );
  lens_border_width = parseInt(lens_border_width);

  setTimeout(function () {
    /* Set initial background properties for the result after delay to prevent error*/

    //cx and cy adjusted to better visually fit zoom and lens
    cx = (1.11 * result.offsetWidth) / lens.offsetWidth;
    cy = (1.02 * result.offsetHeight) / lens.offsetHeight;

    //initialize zoomed picture
    result.style.backgroundImage = "url('" + img.src + "')";
    result.style.backgroundSize =
      img.width * cx + "px " + img.height * cy + "px";
    result.style.backgroundRepeat = "no-repeat";
    result.style.backgroundPosition =
      "-" + (arrow_width / 2) * cx + "px -" + (arrow_height / 2) * cy + "px";

    //initialize arrow
    document.body.append(arrow);
    reset_zoom_n_arrow();
    arrow.style.visibility = "visible";
  }, 500);

  /*move the lens and result image according to the mouse position
 currently bottom and top fencing is sub optimal, but gets the job done*/
  function moveAt(pageX, pageY, clientX, clientY) {
    let x = pageX;
    let y = pageY;
    let imgLeft = img.getBoundingClientRect().left;
    let imgRight = img.getBoundingClientRect().right;
    let imgTop = img.getBoundingClientRect().top;

    /* Prevent the lens from being positioned outside the image: 
    +5/-5 to allow smoother transition upon reenabling the movement
    as soon as virtual fence is touched, movement disabled and slight reset inwards to img*/

    if (x > imgRight - lens.offsetWidth - lens_border_width) {
      //right border
      x = imgRight - lens.offsetWidth - lens_border_width - 20;
      document.removeEventListener("mousemove", onMouseMove);
      document.removeEventListener("touchmove", onTouchMove);
    }
    if (x < imgLeft + lens_border_width) {
      // left border
      x = imgLeft + lens_border_width + 20;
      document.removeEventListener("mousemove", onMouseMove);
      document.removeEventListener("touchmove", onTouchMove);
    }
    if (lens.getBoundingClientRect().y - 20 < img.getBoundingClientRect().y) {
      //upper border
      y += 20;
      document.removeEventListener("mousemove", onMouseMove);
      document.removeEventListener("touchmove", onTouchMove);
    }

    if (
      lens.getBoundingClientRect().bottom + 20 >
      img.getBoundingClientRect().bottom
    ) {
      // lower border
      y -= 20;
      document.removeEventListener("mousemove", onMouseMove);
      document.removeEventListener("touchmove", onTouchMove);
    }

    arrow.style.left = x - arrow_width / 2 + "px";
    arrow.style.top = y - arrow_height / 2 + "px";

    const mouseXRelativeToImage = clientX - imgLeft;
    const mouseYRelativeToImage = clientY - imgTop;

    result.style.backgroundPosition =
      "-" +
      mouseXRelativeToImage * cx +
      "px -" +
      mouseYRelativeToImage * cy +
      "px";
  }

  //disables the browser default drag animation
  arrow.ondragstart = function () {
    return false;
  };

  function onMouseMove(event) {
    moveAt(event.pageX, event.pageY, event.clientX, event.clientY);
  }

  function onTouchMove(event) {
    event.preventDefault();
    var touch = event.touches[0] || event.changedTouches[0];
    moveAt(touch.pageX, touch.pageY, touch.clientX, touch.clientY);
  }

  //differentieate between mouse and touch events
  arrow.onmousedown = arrow.ontouchstart = function (event) {
    event.preventDefault();

    if (event.type === "touchstart") {
      document.addEventListener("touchmove", onTouchMove);
      document.addEventListener("touchend", stopDrag);
    } else {
      document.addEventListener("mousemove", onMouseMove);
      document.addEventListener("mouseup", stopDrag);
    }

    function stopDrag() {
      document.removeEventListener("mousemove", onMouseMove);
      document.removeEventListener("mouseup", stopDrag);
      document.removeEventListener("touchmove", onTouchMove);
      document.removeEventListener("touchend", stopDrag);
    }
  };

  //reset zoom and moveArrow+Lens to initial position
  reset_zoom_n_arrow = function () {
    if (img && arrow) {
      const imgRect = img.getBoundingClientRect();

      arrow.style.left = imgRect.left + window.scrollX + "px";
      arrow.style.top = imgRect.top + window.scrollY + "px";

      result.style.backgroundPosition =
        "-" + (arrow_width / 2) * cx + "px -" + (arrow_height / 2) * cy + "px";
    }
  };

  // reset zoom on window change or manual by user if any glitches occur
  document
    .getElementById("zoom_reset_btn")
    .addEventListener("click", reset_zoom_n_arrow);

  window.onresize = reset_zoom_n_arrow;
}

let digiHasStarted = false;
window.onbeforeunload = function (e) {
  if (digiHasStarted) {
    // placeholder message, not shown in modern browsers
    return "Are you sure you want to leave this page?  You will lose any unsaved data.";
  }
};

function errorOccured() {
  if (!document.getElementById("hww_container")) {
    document.getElementById("digi_area").innerHTML = `
    <div class="hww_placeholder_container" id="hww_container"></div>`;
  }

  document.getElementById("hww_container").innerHTML = `
  <span class="head2">Whoops</span>
  <p>Something went wrong, please try again <br>
  If the problems persists, please contact us.</p>`;

  document.getElementById("digi_area").scrollIntoView();
}

async function PrepareDataset() {
  // catch case where user continues with next digitization
  var ele;
  if (document.getElementById("hww_container")) {
    ele = document.getElementById("hww_container");
  } else {
    ele = document.getElementById("thank_item");
  }

  if (isEnglishUrl()) {
    ele.innerHTML = `
    <span class="loader"></span>
    <p>Preparing the dataset</p>`;
  } else {
    ele.innerHTML = `
    <span class="loader"></span>
    <p>Daten werden vorbereiet</p>`;
  }

  const wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
  const startTime = Date.now();

  let attempts = 0;
  let success = false;
  let image_src, month_short, days, month_long;

  while (attempts < 5 && !success) {
    try {
      attempts++;
      const response = await fetch(
        "https://doit-lwi.tu-bs.de/api/data/hww-precip"
      );
      const data = await response.json();

      if (isEnglishUrl()) {
        month_long = data.month_en;
        month_short = data.month_short_en;
      } else {
        month_long = data.month_de;
        month_short = data.month_short_de;
      }

      image_src = data.file_path;
      days = new Date(data.year, data.month_num + 1, 0).getDate();

      dataset_id = data.data_id;
      dataset_iv = data.data_iv;
      success = true;
    } catch (error) {
      console.error(`Error on attempt ${attempts}:`, error);
      await wait(2000); // Wait 2 seconds before retrying
    }
  }

  if (!success) {
    console.error("Failed to load the dataset after 5 attempts.");
    errorOccured();
    return;
  }

  // Ensure at least 2.5 seconds have elapsed before proceeding
  // makes website feel more engaging
  const elapsedTime = Date.now() - startTime;
  const remainingTime = Math.max(0, 2500 - elapsedTime);

  if (remainingTime > 0) {
    await wait(remainingTime);
  }

  try {
    const response = await fetch("./hww-inner.html");
    const html = await response.text();
    const parser = new DOMParser();
    const doc = parser.parseFromString(html, "text/html");

    doc.getElementById("fullsize").src = image_src;
    doc.getElementById("digi_month").innerHTML = month_long;

    const manipulatedHtml = doc.body.innerHTML;
    const digi_area = document.getElementById("digi_area");
    digi_area.classList.remove("center_cont__w60");
    digi_area.classList.add("center_cont__w90");
    digi_area.innerHTML = manipulatedHtml;
  } catch (error) {
    console.error("Error loading hww-inner.html:", error);
    errorOccured();
    return;
  }

  try {
    buildAndHandleDigitizationForm(days, month_short);
    imageZoom("fullsize", "zoom", "lens");
    document.getElementById("digi_area").scrollIntoView();
  } catch (error) {
    console.error("Error building the digitization form:", error);
    errorOccured();
    return;
  }

  // Add helper class to enable CSS consistency
  document
    .querySelector(".digitization_wrapper")
    .classList.add("is-button-pressed");

  // Activate the onbeforeunload event
  digiHasStarted = true;
}

document.getElementById("start_digi").addEventListener("click", PrepareDataset);
//dev only
// document.addEventListener("DOMContentLoaded", PrepareDataset);
