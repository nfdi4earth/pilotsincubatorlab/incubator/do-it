# Digitization of analogue records in time (DO IT!)

## References

See the project outcome on [https://doit-lwi.tu-bs.de](https://doit-lwi.tu-bs.de) and a more elaborate description of what the project is about on [youtube](https://www.youtube.com/watch?v=rtIMP1iuMUU).

![Title Image](title-image.png)

Records of environmental observations exist for decades before the digital era either as hand-written values or registrations from measuring instruments. Unfortunately, these data are most often not digitized yet and hence are not accessible, although it would be highly valuable for long-term analyses, trend detections or similar. Usually the data are digitized manually by internships, student assistants or employees if there is some spare time. Due to the required manpower, this way of digitization will take several decades to be completed. Furthermore, the quality of paper records can degrade significantly over time. In many places, there is a risk of losing these unique data in the cellars of public authorities. The novelty of our approach is to enable the involvement of numerous volunteers without scientific background (citizen scientists =CS) by breaking down the time-consuming part of the digitization to an easy applicable smartphone application. By providing scans of the analogue data the CS can type the hand-written numbers into a standardized table or retrace the recorded graph with their fingers. Beside the digitalized data, the most important outcome of the project will be the developed smartphone app, which can flexibly handle different types of data and thus be transferred to other scientific fields.

## Abstract

Thousands years of analogue records exist in earth system sciences, but are still an unburied treasure in the age of digitization. The digitization of these data needs manpower – but not necessarily with scientific background. Breaking down the digitization to an easy applicable smartphone application enables the involvement of citizen scientists to overcome the manpower bottleneck. This app can be applicable to all kind of analogue data and hence useful for numerous scientific fields.

## Server strucutre sketch

![server structure](server_sketch.png)

## Outcomes and Trends

Sourcecode for the website is obtainable in the [/app folder](./app/) of this repo. Notice that this is only the plain html/css/js content as we don't push code of webserver, api and database into this public repository due to security reasons.

The website is accessible from every device via [https://doit-lwi.tu-bs.de](https://doit-lwi.tu-bs.de). In its core functionalities, the app is usable on both mobile devices and PCs. However, it is currently optimized only for PCs and a Full HD resolution.

Through the incubator project, we were able to develop a functioning concept from an idea and implement it as a web app. The website is fully operational and can be used by users. Currently, contrary to the original idea, the functionality is limited to the digitization of tabular content. This is the result of our conceptualization process. The digitization of hydrographs is inherently very complex, so it was more effective to first build the entire system and incorporate the tabular data in order to have a working concept. <br>
Nevertheless, a functioning prototype for the hydrograph digitization exists to demonstrate the concept. This prototype is integrated into the website (password-protected area, contact us via hydriv-digitization@tu-braunschweig.de) and can also be executed as HTML (see [/hydrographProto folder](./hydrographProto/)) . <br>
Building on this foundation, we want to further develop the app (mature version of hydrograph processing) and improve it in general (optimization for smartphones, general UI/UX improvements). Additionally, we want to use the app in further projects to better capture its functionality.

## Funding

This work has been funded by the German Research Foundation (NFDI4Earth,
DFG project no. 460036893, https://www.nfdi4earth.de/).

## TODO

- add citation
- update with snow-data
- provide publication DOI
