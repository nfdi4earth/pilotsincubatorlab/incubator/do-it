# Do It Hydrodraph prototype

Working prototype to explore and highlight the problems with such an implementation.

The current version has been thoroughly tested, so you shouldn’t run into any errors during normal use. However, if you stray from the usual way of using it or start experimenting with unexpected sequences of actions, you might encounter some issues. For instance, you could see the magnifying glass pop up when you don’t want it or have a toggle that doesn’t turn off properly.
